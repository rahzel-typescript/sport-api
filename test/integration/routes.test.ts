import request from 'supertest';
import { app, server } from '../../src/index';

describe('Test Routes (integration)', () => {
  let testSportId: string;
  let testEventId: string;

  beforeAll(async () => {
    const res: any = await request(app).get('/api/sports');
    testSportId = res.body[0].id;
    testEventId = res.body[0].comp[0].events[0].id;
  });

  afterAll(() => {
    server.close();
  });

  describe('list all sports', () => {
    it('should return all sports as json', async () => {
      const res = await request(app).get('/api/sports');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body[0]).toHaveProperty('comp');
      expect(res.body[0]).toHaveProperty('desc', 'Football');
    });

    it('should return all sports as json in german if specified in url', async () => {
      const res = await request(app).get('/api/de/sports');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body[0]).toHaveProperty('comp');
      expect(res.body[0]).toHaveProperty('desc', 'Fußball');
    });
  });

  describe('list all events(per sportId)', () => {
    it('should return all events as json', async () => {
      const res = await request(app).get('/api/sports/events');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body[0]).toHaveProperty('event_type');
    });

    it('should return all events as json for a given sportId', async () => {
      const res = await request(app).get(`/api/sports/${testSportId}/events`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body[0]).toHaveProperty('event_type');
      expect(res.body[0]).toHaveProperty('sport_id', testSportId);
    });
  });

  describe('list all data for a given event', () => {
    it('should return all data for a given event', async () => {
      const res = await request(app).get(`/api/sports/events/${testEventId}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).not.toBeInstanceOf(Array);
      expect(res.body).toHaveProperty('event_type');
    });

    it('should return all data for an event for a sport id', async () => {
      const res = await request(app).get(`/api/sports/${testSportId}/events/${testEventId}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).not.toBeInstanceOf(Array);
      expect(res.body).toHaveProperty('sport_id', testSportId);
    });

    it('should return no event found if no events found', async () => {
      const eventId = 1726595800111111111111111111111111111;
      const res = await request(app).get(`/api/sports/${testSportId}/events/${eventId}`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBe('no event found');
    });
  });

  describe('list all sports in all languages', () => {
    it('list all sports in all languages', async () => {
      const res = await request(app).get(`/api/sports/allLang`);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.length).toBe(3);
    });
  });
});