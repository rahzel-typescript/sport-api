import { verifyCache } from './../../../src/middleware/verifyCache';
import { Request, Response } from 'express-serve-static-core';
import { cache } from '../../../src/dependencies';
describe('Test VerifyCache', () => {
  const path = 'test path';
  const req = { path } as Request;
  const mockJson = jest.fn();
  const res = { json: mockJson } as unknown as Response;
  const next = jest.fn();

  const cacheHasSpy = jest.spyOn(cache, 'has');
  const cacheGetSpy = jest.spyOn(cache, 'get');

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should return cached value if available in cache', () => {
    const cachedResponse = {};
    cacheHasSpy.mockReturnValueOnce(true);
    cacheGetSpy.mockReturnValueOnce(cachedResponse);

    verifyCache(req, res, next);

    expect(cacheHasSpy).toBeCalledTimes(1);
    expect(cacheGetSpy).toBeCalledWith(path);
    expect(mockJson).toBeCalledWith(cachedResponse);

    expect(next).not.toBeCalled();
  });

  it('should continue calling next middleware if value not in cache ', () => {
    cacheHasSpy.mockReturnValueOnce(false);

    verifyCache(req, res, next);

    expect(cacheHasSpy).toBeCalledWith(path);
    expect(next).toBeCalledTimes(1);

    expect(cacheGetSpy).not.toBeCalled();
  });

  it('should throw error if caching fails', () => {
    const cacheError = Error('cache Error');

    cacheHasSpy.mockImplementation(() => {
      throw cacheError;
    });

    try {
      verifyCache(req, res, next);
    } catch (err) {
      expect(err).toBe(cacheError);
    }

    expect(next).toBeCalledTimes(1);
    expect(cacheGetSpy).not.toBeCalled();
  });
});