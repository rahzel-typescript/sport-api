import { Sport, Competition, Event } from './../../src/types';
import { getEventsFromSports } from '../../src/utils';

describe('Test Utils', () => {
  describe('getEventsFromSports', () => {
    const event: Event = { id: 3, desc: 'test event', event_type: 'some type' };
    const event2: Event = { id: 33, desc: 'test event 2', event_type: 'some type' };
    const event3: Event = { id: 333, desc: 'test event 3', event_type: 'some type' };

    const competition: Competition = { id: 2, desc: 'test comp', events: [event, event2] };
    const competition2: Competition = { id: 22, desc: 'test comp 2', events: [event3] };

    const sport: Sport = { id: 1, comp: [competition] };
    const sport2: Sport = { id: 11, comp: [competition2] };

    const sports = [sport, sport2];

    it('should get Events from Sports', () => {
      const result = getEventsFromSports(sports);
      expect(result).toEqual([event, event2, event3]);
    });
  });
});