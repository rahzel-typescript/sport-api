import { Language } from './../../../src/types';
import { getSports } from '../../../src/services/web-api-service';
import axios from 'axios';
import { Sport } from '../../../src/types';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('Test Web-Api-Service', () => {
  const env = process.env;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...env };
  });

  afterEach(() => {
    jest.clearAllMocks();
    process.env = env;
  });

  const sport: Sport = { id: 1, comp: [] };
  const mockSports: Sport[] = [sport];

  const axiosSuccessResponse = {
    data: {
      result: {
        sports: mockSports
      }
    }
  };

  it('should return with sports in default language if called without params', async () => {
    process.env.DEFAULT_LANGUAGE = 'en';
    process.env.BV_BASE_URL = 'url1';
    process.env.BV_EVENTS_URL = 'url2';

    const url = 'url1/en/url2';
    mockedAxios.get.mockResolvedValueOnce(axiosSuccessResponse);

    const result = await getSports();

    expect(result).toBe(mockSports);
    expect(mockedAxios.get).toBeCalledWith(url);
  });

  it('should return with sports on a language specified in params', async () => {
    process.env.DEFAULT_LANGUAGE = 'en';
    process.env.BV_BASE_URL = 'url1';
    process.env.BV_EVENTS_URL = 'url2';

    const otherLang: Language = 'de';

    const url = 'url1/' + otherLang + '/url2';
    mockedAxios.get.mockResolvedValueOnce(axiosSuccessResponse);

    const result = await getSports(otherLang);

    expect(result).toBe(mockSports);
    expect(mockedAxios.get).toBeCalledWith(url);
  });

  it('should return with empty array if unable to retrieve data', async () => {
    mockedAxios.get.mockRejectedValueOnce('axi error');

    const result = await getSports();

    expect(result).toEqual([]);
  });
});