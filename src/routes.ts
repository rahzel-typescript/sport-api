import { cache } from './dependencies';
import { getEventsFromSports } from './utils';
import { getSports } from './services/web-api-service';
import { Language, Sport, Competition, Event } from './types';
import { Router } from 'express';
import { verifyCache } from './middleware/verifyCache';

const routes = Router();

// list all sports
routes.get('/:lang(en|de|zh)?/sports', verifyCache, async (req, res) => {
  const sports = await getSports(req.params.lang as Language);
  cache.set(req.path, sports);
  res.json(sports);
});

// list all events(per sportId) – where sportId is optional parameter.
routes.get('/:lang(en|de|zh)?/sports/:sportId?/events', verifyCache, async (req, res) => {
  const sportId = req.params.sportId ? +req.params.sportId : undefined;
  const sports = await getSports(req.params.lang as Language);
  let events;

  if (sportId) {
    const sport = sports.find((sp: Sport) => sp.id === sportId);
    events = sport?.comp.flatMap((comp: Competition) => comp.events);
  } else {
    events = getEventsFromSports(sports);
  }
  cache.set(req.path, events);

  res.json(events);
});

// list all data for a given event
routes.get('/:lang(en|de|zh)?/sports/:sportId?/events/:eventId', verifyCache, async (req, res) => {
  const sportId = req.params.sportId ? +req.params.sportId : undefined;
  const eventId = +req.params.eventId;
  let events;

  const sports = await getSports(req.params.lang as Language);

  if (sportId) {
    const sport = sports.find((sp: Sport) => sp.id === sportId);
    events = sport?.comp.flatMap((comp: Competition) => comp.events);
  } else {
    events = getEventsFromSports(sports);
  }

  const event = events?.find((e: Event) => e.id === eventId);
  cache.set(req.path, event);

  if (event) {
    res.json(event);
  } else {
    res.json('no event found');
  }
});

// list all sports in all languages
routes.get('/sports/allLang', verifyCache, async (req, res) => {
  const allLanguage: Language[] = ['en', 'de', 'zh'];
  const result = await Promise.all(allLanguage.map(async (lang) => getSports(lang)));
  cache.set(req.path, result);

  res.json(result);
});

export default routes;