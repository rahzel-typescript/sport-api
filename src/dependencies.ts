import NodeCache from 'node-cache';
const { CACHE_TTL_SEC } = process.env;

const ttl = CACHE_TTL_SEC ? +CACHE_TTL_SEC : 15;

export const cache = new NodeCache({ stdTTL: ttl });