import { Competition, Event, Sport } from './types';

const getEventsFromSports = (sports: Sport[]) => sports.reduce<Event[]>((acc, curr) => {
  curr.comp.map((competition: Competition) => acc.push(...competition.events));
  return acc;
}, []);

export {
  getEventsFromSports
};