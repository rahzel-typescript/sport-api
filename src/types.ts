export type Language = 'en' | 'de' | 'zh';

export type Sport = {
  id: number
  comp: Competition[]
};

export type Competition = {
  id: number
  desc: string
  events: Event[]
};

export type Event = {
  id: number
  desc: string
  event_type: string
  oppADesc?: string,
  oppBDesc?: string,
  markets?: Market[]
};

export type Response = {
  status: {
    success: boolean
    errorCode: 0
    extraInfo: {}
  },
  result: {
    transitions_pgate_path: string
    total_number_of_events: number
    sports: Sport[]
  }
};

export type Market = {
  id: number
  event_type: string
  desc: string
};

export type Outcome = {
  id: number
  event_type: string
  desc: string
};