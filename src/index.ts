import 'dotenv/config';
import express from 'express';
import routes from './routes';

export const app = express();
const port = 8080;

app.use('/api', routes);

export const server = app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});