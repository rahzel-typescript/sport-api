import { Handler } from 'express';
import { cache } from '../dependencies';

export const verifyCache: Handler = (req, res, next) => {
  try {
    if (cache.has(req.path)) {
      return res.json(cache.get(req.path));
    }
    next();
  } catch (err: any) {
    console.log(err);
    next();
  }
};