import { Language, Sport } from './../types';
import axios from 'axios';

const getSports = async (lang = process.env.DEFAULT_LANGUAGE as Language): Promise<Sport[]> => {
  const { BV_BASE_URL, BV_EVENTS_URL } = process.env;
  try {
    const { data } = await axios.get(`${BV_BASE_URL}/${lang}/${BV_EVENTS_URL}`);
    return data.result.sports;
  } catch (exception) {
    console.log('unable to retrieve data', exception);
    return [];
  }
};

export {
  getSports
};