Node.js API server that calls the following URL and navigates through it's content.

project is using dotenv. example .env file:

BV_BASE_URL="https://partners.betvictor.mobi"
BV_EVENTS_URL="in-play/1/events"
DEFAULT_LANGUAGE="en"
CACHE_TTL_SEC=15



* Endpoint to list all sports: localhost:8080/api/<lang?>/sports
* Endpoint to list all events(per sportId) – where sportId is optional parameter: localhost:8080/api/<lang?>/sports/<sportId?>/events
* Endpoint to list all data for a given event: localhost:8080/api/<lang?>/sports/<sportId?>/events/eventId
* Endpoint to list all sports in all languages: localhost:8080/api/sports/allLang
* Language support (English, German and Chinese): lang(en|de|zh)?                 
* Caching: 15ms default
* Full test coverage:

![image.png](./image.png)
